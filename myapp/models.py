from .app import db
from flask_login import UserMixin

class Author(db.Model):
    """
    Classe Auteur
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)
class User(db.Model):
	username = db.Column(db.String(50),primary_key=True)
	password = db.Column(db.String(64))

	def get_id(self):
		return self.username

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Float)
    title = db.Column(db.String(120))
    url = db.Column(db.String(250))
    img = db.Column(db.String(90))
    author_id = db.Column(db.Integer,
                          db.ForeignKey("author.id"))
    author = db.relationship("Author",
                             backref=db.backref("books", lazy="dynamic"))

    def __repr__(self):
      return "<Book (%d) %s>" % (self.id, self.name)


def get_sample():
  return Book.query.limit(10).all()

def get_author(id):
	return Author.query.get_or_404(id)

def exist_author(name):
    return Author.query.filter(Author.name==name).one()

def get_book_detail(bookid):
    return Book.query.get_or_404(bookid)
