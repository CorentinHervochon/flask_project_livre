from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os.path
from flask_bootstrap import Bootstrap

app=Flask(__name__)
app.debug=True
Bootstrap(app)
app.config["BOOTSTRAP_SERVE_LOCAL"] = True

def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),p))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///"+mkpath("../myapp.db"))

app.config["SECRET_KEY"]="1d514b1c-a3ae-9ec2-5fe9d2b6a31c"

db = SQLAlchemy(app)
